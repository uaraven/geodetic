/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic.elevation;

import net.ninjacat.geodetic.Location;

/**
 * Interface to retrieve elevation data from Digital Elevation Models like SRTM3 or ASTER GDEM
 */
public interface Dem {

    /**
     * Returns elevation of the location.
     *
     * @param location location on Earth
     * @return ground level elevation in meters
     */
    double getElevation(Location location);

    /**
     * Checks whether supplied elevation is invalid. Invalid value can differ from model to model
     *
     * @param elevation elevation to check
     * @return validity of the elevation value
     */
    boolean isInvalid(double elevation);

    /**
     * Closes all files that might be used by the model
     */
    void close();
}
