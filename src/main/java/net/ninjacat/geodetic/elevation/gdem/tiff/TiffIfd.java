/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic.elevation.gdem.tiff;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

class TiffIfd {

    private static final int IFD_ENRTY_LENGTH = 12;
    private final Map<Integer, IfdEntry> entries = new ConcurrentHashMap<Integer, IfdEntry>();
    private final int nextIfdOffset;

    TiffIfd(EndiannesAwareFileReader file, int offset) throws IOException {
        file.seek(offset);
        readIfd(file);
        nextIfdOffset = file.readInt();
    }

    private static IfdEntry readIfdEntry(EndiannesAwareFileReader file) throws IOException {
        long filePointer = file.getFilePointer();
        try {
            short tag = file.readShort();
            IftFieldType type = IftFieldType.getByType(file.readShort());
            int count = file.readInt();
            int offset = file.readInt();
            return new IfdEntry(tag, type, count, offset);
        } catch (TiffException ignored) {
            file.seek(filePointer + IFD_ENRTY_LENGTH);
            return IfdEntry.INVALID;
        }
    }

    public Map<Integer, IfdEntry> getEntries() {
        return Collections.unmodifiableMap(entries);
    }

    public int getNextIfdOffset() {
        return nextIfdOffset;
    }

    private void readIfd(EndiannesAwareFileReader file) throws IOException {
        int count = file.readUnsignedShort();
        for (int i = 0; i < count; i++) {
            IfdEntry ifdEntry = readIfdEntry(file);
            if (!ifdEntry.isInvalid()) {
                entries.put(ifdEntry.getTag(), ifdEntry);
            }
        }
    }

}
