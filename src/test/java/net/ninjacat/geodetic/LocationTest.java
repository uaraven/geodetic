/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@SuppressWarnings({"TypeMayBeWeakened", "NonBooleanMethodNameMayNotStartWithQuestion"})
public class LocationTest {

    private static final double EPSILON = 0.00001;

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailToCreateWhenNoGeodeticSystemIsSupplied() throws Exception {
        new Location(null, 0, 0, 0);
    }

    @Test
    public void locationsWithDifferentGeodeticSystemsShouldNotBeEqual() throws Exception {
        Location loc1 = new Location(GeodeticSystem.WGS84, 20, -20, 0);
        Location loc2 = new Location(GeodeticSystem.SK42, 20, -20, 0);

        assertFalse("Locations with different geodetic systems should not be equal", loc1.equals(loc2));
    }

    @Test
    public void locationsWithDifferentAltitudeShouldNotBeEqual() throws Exception {
        Location loc1 = new Location(GeodeticSystem.WGS84, 20, -20, 0);
        Location loc2 = new Location(GeodeticSystem.WGS84, 20, -20, 20);

        assertFalse("Locations with different altitude should not be equal", loc1.equals(loc2));
    }

    @Test
    public void locationsWithDifferentLatitudeShouldNotBeEqual() throws Exception {
        Location loc1 = new Location(GeodeticSystem.WGS84, 30, -20, 20);
        Location loc2 = new Location(GeodeticSystem.WGS84, 20, -20, 20);

        assertFalse("Locations with different latitude should not be equal", loc1.equals(loc2));
    }

    @Test
    public void locationsWithDifferentLongitudeShouldNotBeEqual() throws Exception {
        Location loc1 = new Location(GeodeticSystem.WGS84, 20, 20, 20);
        Location loc2 = new Location(GeodeticSystem.WGS84, 20, -20, 20);

        assertFalse("Locations with different longitude should not be equal", loc1.equals(loc2));
    }

    @Test
    public void sameLocationsShouldBeEqual() throws Exception {
        Location loc1 = new Location(GeodeticSystem.WGS84, 20, -20, 20);
        Location loc2 = new Location(GeodeticSystem.WGS84, 20, -20, 20);

        assertEquals("Same locations should be equal", loc1, loc2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailForDifferentGeodeticSystems() throws Exception {
        Location start = Location.wgs84(54.98333299999999, 73.366667);
        Location target = new Location(GeodeticSystem.SK42, 55.39468524, 72.75800662, 0);

        start.getDistanceAndBearing(target);
    }

    @Test
    public void shouldCalculateDistanceToOtherLocation() throws Exception {
        Location start = Location.wgs84(54.98333299999999, 73.366667);
        Location target = Location.wgs84(55.39468524, 72.75800662);

        DistanceAndBearing distanceAndBearing = start.getDistanceAndBearing(target);

        assertEquals("Distance should be equal", 60000, distanceAndBearing.getDistance(), EPSILON);
        assertEquals("Initial bearing should be equal", 320, distanceAndBearing.getInitialBearing(), EPSILON);
        assertEquals("Final bearing should be equal", 319.5002609, distanceAndBearing.getFinalBearing(), EPSILON);
    }

    @Test
    public void shouldCalculateDistanceToOtherLocationShortDistance() throws Exception {
        Location start = Location.wgs84(50.4501, 30.523400000000038);
        Location target = Location.wgs84(50.46746662, 30.53069087);

        DistanceAndBearing distanceAndBearing = start.getDistanceAndBearing(target);

        assertEquals("Distance should be equal", 2000, distanceAndBearing.getDistance(), 0.001);
        assertEquals("Initial bearing should be equal", 15, distanceAndBearing.getInitialBearing(), EPSILON);
        assertEquals("Final bearing should be equal", 15.00562247, distanceAndBearing.getFinalBearing(), EPSILON);
    }

    @Test
    public void shouldFindLocationByDistanceAndBearing() throws Exception {
        Location start = Location.wgs84(50.4501, 30.5234);
        Location expected = Location.wgs84(50.538675, 30.547975);

        DistanceAndBearing distanceAndBearing = new DistanceAndBearing(10000, 10, 0);

        Location actual = start.getLocationByDistanceAndBearing(distanceAndBearing);

        assertEquals("Invalid calculated latitude", expected.getLatitude(), actual.getLatitude(), 0.0001);
        assertEquals("Invalid calculated longitude", expected.getLongitude(), actual.getLongitude(), 0.0001);
    }

    @Test
    public void shouldFindLocationByTwoObservers() throws Exception {
        Location observer1 = Location.wgs84(50, 40);
        double bearing1 = 27;

        Location observer2 = Location.wgs84(50.071065, 39.991255);
        double bearing2 = 139.97;

        Location expected = Location.wgs84(50.040066, 40.03179);

        Location actual = Location.getLocationByIntersection(observer1, bearing1, observer2, bearing2);

        assertEquals("Invalid calculated latitude", expected.getLatitude(), actual.getLatitude(), 0.00001);
        assertEquals("Invalid calculated longitude", expected.getLongitude(), actual.getLongitude(), 0.00001);
    }

    @Test
    public void shouldFindLocationByTwoObserversLargeDistanceIntersection() throws Exception {
        Location observer1 = Location.wgs84(50, 40);
        double bearing1 = 40;

        Location observer2 = Location.wgs84(50.190663, 42.220707);
        double bearing2 = 296.08;

        Location expected = Location.wgs84(50.617250, 40.820075);

        Location actual = Location.getLocationByIntersection(observer1, bearing1, observer2, bearing2);

        assertEquals("Invalid calculated latitude", expected.getLatitude(), actual.getLatitude(), 0.0001);
        assertEquals("Invalid calculated longitude", expected.getLongitude(), actual.getLongitude(), 0.0001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailtToFindIntersectionForObserversWithDifferentGS() throws Exception {
        Location observer1 = Location.wgs84(50, 40);
        Location observer2 = new Location(GeodeticSystem.SK42, 50.190663, 42.220707, 0);
        Location.getLocationByIntersection(observer1, 1, observer2, 359);
    }
}
