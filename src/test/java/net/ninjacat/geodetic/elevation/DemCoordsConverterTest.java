/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic.elevation;

import net.ninjacat.geodetic.elevation.common.DemCoordsConverter;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DemCoordsConverterTest {

    @Test
    public void shouldExtractSecondsFromDegree() throws Exception {
        double degrees = 50.41635777777777777;

        DemCoordsConverter converter = new DemCoordsConverter(3, 1201);
        double seconds = converter.extractSeconds(degrees);

        assertEquals("converter should be correct", 1498.888, seconds, 0.0001);
    }

    @Test
    public void shouldConvertDegreeToLinearCoord() throws Exception {
        double degrees = 50.41635777777777777;

        DemCoordsConverter converter = new DemCoordsConverter(3, 1201);
        double linearCoord = converter.toLinearCoord(converter.extractSeconds(degrees));

        assertEquals("linearCoord should be correct", 499, linearCoord, 0.0001);
    }

    @Test
    public void shouldCalculateInterpolationFactor() throws Exception {
        double degrees = 50.41635777777777777;

        DemCoordsConverter converter = new DemCoordsConverter(3, 1201);
        double interp = converter.interpolationFactor(converter.extractSeconds(degrees));

        assertEquals("interp should be correct", 0.6293, interp, 0.0001);

    }
}
