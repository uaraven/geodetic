/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic.converters;

import net.ninjacat.geodetic.GeodeticConverter;
import net.ninjacat.geodetic.GeodeticSystem;
import net.ninjacat.geodetic.Location;

import static java.lang.Math.*;
import static net.ninjacat.geodetic.GeodeticSystem.SK42;
import static net.ninjacat.geodetic.GeodeticSystem.WGS84;

/**
 * Using formulae from http://gis-lab.info/qa/wgs84-sk42-wgs84-formula.html
 */
public abstract class Wgs84Sk42Common implements GeodeticConverter {

    protected static final double ro = 206264.8062; // arc converter per radian

    protected static final double a = (WGS84.getSemiMajorAxis() + SK42.getSemiMajorAxis()) / 2;
    protected static final double e2 = (WGS84.getE2() + SK42.getE2()) / 2;
    protected static final double da = WGS84.getSemiMajorAxis() - SK42.getSemiMajorAxis();
    protected static final double de2 = WGS84.getE2() - SK42.getE2();

    protected static final double dx = 23.92;
    protected static final double dy = -141.27;
    protected static final double dz = -80.9;

    protected static final double wx = 0;
    protected static final double wy = 0;
    protected static final double wz = 0;

    protected static final double ms = 0;

    public static double sq(double value) {
        return value * value;
    }

    protected void assertGeodeticSystem(Location location, GeodeticSystem requiredSystem) {
        if (location.getGeodeticSystem() != requiredSystem) {
            throw new IllegalArgumentException(String.format("Expected coordinates in %s geodetic system", requiredSystem));
        }
    }

    protected double dLat(Location location) {
        double latr = toRadians(location.getLatitude());
        double lonr = toRadians(location.getLongitude());
        double M = a * (1 - e2) / pow(1 - e2 * sq(sin(latr)), 1.5);
        double N = getN(latr);
        return ro / (M + location.getAltitude()) * (N / a * e2 * sin(latr) * cos(latr) * da + (sq(N) / sq(a) + 1) * N
                * sin(latr) * cos(latr) * de2 / 2 - (dx * cos(lonr) + dy * sin(lonr)) * sin(latr) + dz * cos(latr))
                - wx * sin(lonr) * (1 + e2 * cos(2 * latr))
                + wy * cos(lonr) * (1 + e2 * cos(2 * latr)) - ro * ms * e2 * sin(latr) * cos(latr);
    }

    protected double dLon(Location location) {
        double latr = toRadians(location.getLatitude());
        double lonr = toRadians(location.getLongitude());
        double N = getN(latr);
        return ro / ((N + location.getAltitude()) * cos(latr)) * (-dx * sin(lonr) + dy * cos(lonr)) +
                tan(latr) * (1 - e2) * (wx * cos(lonr) + wy * sin(lonr)) - wz;
    }

    protected double dAlt(Location location) {
        double latr = toRadians(location.getLatitude());
        double lonr = toRadians(location.getLongitude());
        double N = getN(latr);
        return -a / N * da + N * sq(sin(latr)) * de2 / 2 + (dx * cos(lonr) + dy * sin(lonr)) * cos(latr) + dz * sin(latr) -
                N * e2 * sin(latr) * cos(latr) * (wx / ro * sin(lonr)
                        - wy / ro * cos(lonr)) + (sq(a) / N + location.getAltitude()) * ms;
    }

    private double getN(double lat) {
        return a * pow(1 - e2 * sq(sin(lat)), -0.5);
    }
}
