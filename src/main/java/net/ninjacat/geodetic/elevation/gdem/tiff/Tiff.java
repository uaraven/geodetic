/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic.elevation.gdem.tiff;

import java.io.Closeable;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

public class Tiff implements Closeable {

    private final EndiannesAwareFileReader file;
    private final TiffHeader header;
    private final List<TiffIfd> ifds;

    /**
     * @param fileName Name of TIFF file
     * @throws IOException   if I/O Error occurs
     * @throws TiffException if TIFF is invalid
     */
    public Tiff(String fileName) throws IOException {
        file = new EndiannesAwareFileReader(new RandomAccessFile(fileName, "r"));

        header = new TiffHeader(file);
        ifds = readIfds();

        validate();
    }

    private static int readNumber(IfdEntry entry) {
        switch (entry.getType()) {
            case SHORT:
            case LONG:
            case SSHORT:
                if (entry.getCount() != 1) {
                    throw new TiffException("Cannot read multiple data");
                }
                return entry.getOffset();
        }
        return 0;
    }

    private static int[] convertToIntArray(short[] data) {
        int[] ints = new int[data.length];
        for (int i = 0; i < data.length; i++) {
            ints[i] = data[i];
        }
        return ints;
    }

    public int getWidth() {
        return readNumber(findTag(Tags.ImageWidth));
    }

    public int getHeight() {
        return readNumber(findTag(Tags.ImageLength));
    }

    public int getBitsPerPixel() {
        return readNumber(findTag(Tags.BitsPerSample));
    }

    public int getPixelValue(int x, int y) throws IOException {
        int rowsPerStrip = readNumber(findTag(Tags.RowsPerStrip));
        int strip = y / rowsPerStrip;
        int lineInStrip = y % rowsPerStrip;
        int width = getWidth();

        int[] stripOffsets = readIntArray(findTag(Tags.StripOffsets));
        int[] stripByteCounts = readIntArray(findTag(Tags.StripByteCounts));

        file.seek(stripOffsets[strip]);
        byte[] data = file.readBytes(stripByteCounts[strip]);
        ByteBuffer buffer = ByteBuffer.wrap(data);
        buffer.order(header.isLittleEndian() ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);

        int offset = lineInStrip * width + x;

        int bytesPerPixel = getBitsPerPixel() / 8;
        switch (bytesPerPixel) {
            case 1:
                return data[offset];
            case 2:
                short[] shorts = new short[data.length / 2];
                buffer.asShortBuffer().get(shorts);
                return shorts[offset];
            case 4:
                return buffer.asIntBuffer().get(offset);
            default:
                return 0;
        }
    }

    @Override
    public void close() throws IOException {
        file.close();
        ifds.clear();
    }

    /**
     * @throws TiffException
     */
    private void validate() {
        verifyTag(Tags.ImageWidth, "ImageWidth");
        verifyTag(Tags.ImageLength, "ImageLength");
        verifyTag(Tags.BitsPerSample, "BitsPerSample");
        verifyTag(Tags.RowsPerStrip, "RowsPerStrip");
        verifyTag(Tags.StripByteCounts, "StripByteCounts");
        verifyTag(Tags.StripOffsets, "StripOffsets");
        if (findTag(Tags.Compression).isInvalid() || readNumber(findTag(Tags.Compression)) != 1) {
            throw new TiffException("Unsupported compression");
        }
    }

    private void verifyTag(short tag, String tagName) {
        if (findTag(tag).isInvalid()) {
            throw new TiffException("No " + tagName + " tag");
        }
    }

    private int[] readIntArray(IfdEntry entry) throws IOException {
        if (entry.getType() == IftFieldType.LONG && entry.getCount() > 1) {
            file.seek(entry.getOffset());
            if (entry.getType() == IftFieldType.LONG) {
                return file.readInts(entry.getCount());
            } else {
                short[] data = file.readShorts(entry.getCount());
                return convertToIntArray(data);
            }
        } else {
            throw new TiffException(String.format("Entry %s is not int array", entry));
        }
    }

    private IfdEntry findTag(int tag) {
        for (TiffIfd ifd : ifds) {
            if (ifd.getEntries().containsKey(tag)) {
                return ifd.getEntries().get(tag);
            }
        }
        return IfdEntry.INVALID;
    }

    private List<TiffIfd> readIfds() throws IOException {
        int offset = header.getIfdOffset();
        List<TiffIfd> ifdList = new ArrayList<TiffIfd>();
        while (offset != 0) {
            TiffIfd ifd = new TiffIfd(file, offset);
            offset = ifd.getNextIfdOffset();
            ifdList.add(ifd);
        }
        return ifdList;
    }
}
