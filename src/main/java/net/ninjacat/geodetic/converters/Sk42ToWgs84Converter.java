/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic.converters;

import net.ninjacat.geodetic.GeodeticSystem;
import net.ninjacat.geodetic.Location;

public class Sk42ToWgs84Converter extends Wgs84Sk42Common {

    private static final int MINUTES = 3600;

    @Override
    public Location convert(Location from) {
        assertGeodeticSystem(from, GeodeticSystem.SK42);
        double newLat = from.getLatitude() + dLat(from) / MINUTES;
        double newLon = from.getLongitude() + dLon(from) / MINUTES;
        double newAlt = from.getAltitude() + dAlt(from);
        return new Location(GeodeticSystem.SK42, newLat, newLon, newAlt);
    }
}
