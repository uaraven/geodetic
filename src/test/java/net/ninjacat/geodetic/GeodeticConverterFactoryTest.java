/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic;

import net.ninjacat.geodetic.converters.Sk42ToWgs84Converter;
import net.ninjacat.geodetic.converters.Wgs84ToSk42Converter;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class GeodeticConverterFactoryTest {

    @Test(expected = NoConversionException.class)
    public void shouldFailIfThereIsNoConversion() throws Exception {
        GeodeticConverterFactory.getConverter(GeodeticSystem.SK42, GeodeticSystem.UNKNOWN);
    }

    @Test
    public void shouldReturnWgs84ToSk42Converter() throws Exception {
        GeodeticConverter converter = GeodeticConverterFactory.getConverter(GeodeticSystem.WGS84, GeodeticSystem.SK42);

        assertTrue("Should return WGS84->SK42 converter", converter instanceof Wgs84ToSk42Converter);
    }

    @Test
    public void shouldReturnSk42ToWgs84Converter() throws Exception {
        GeodeticConverter converter = GeodeticConverterFactory.getConverter(GeodeticSystem.SK42, GeodeticSystem.WGS84);

        assertTrue("Should return SK42->WGS84 converter", converter instanceof Sk42ToWgs84Converter);
    }
}
