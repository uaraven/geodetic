/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic;

/**
 * Represents distance and bearing to a point
 */
public class DistanceAndBearing {
    private static final int FULL_CIRCLE = 360;
    private final double distance;
    private final double initialBearing;
    private final double finalBearing;

    /**
     * Creates new instance of distance and bearing.<br>
     * Bearings are normalized to 0-360 degrees.
     *
     * @param distance       distance to a point along geodesic in meters
     * @param initialBearing initial bearing in degrees
     * @param finalBearing   final bearing in degrees
     */
    public DistanceAndBearing(double distance, double initialBearing, double finalBearing) {
        this.distance = distance;
        this.initialBearing = normalize(initialBearing);
        this.finalBearing = normalize(finalBearing);
    }

    /**
     * See {@link DistanceAndBearing#DistanceAndBearing(double, double, double)}. Final bearing is set to zero
     *
     * @param distance
     * @param initialBearing
     */
    public DistanceAndBearing(double distance, double initialBearing) {
        this(distance, initialBearing, 0);
    }

    private static double normalize(double bearing) {
        return (bearing + FULL_CIRCLE) % FULL_CIRCLE;
    }

    public double getDistance() {
        return distance;
    }

    public double getInitialBearing() {
        return initialBearing;
    }

    public double getFinalBearing() {
        return finalBearing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DistanceAndBearing that = (DistanceAndBearing) o;

        if (Double.compare(that.distance, distance) != 0) return false;
        if (Double.compare(that.finalBearing, finalBearing) != 0) return false;
        if (Double.compare(that.initialBearing, initialBearing) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(distance);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(initialBearing);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(finalBearing);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "DistanceAndBearing{" +
                "distance=" + distance +
                ", initialBearing=" + initialBearing +
                ", finalBearing=" + finalBearing +
                '}';
    }
}
