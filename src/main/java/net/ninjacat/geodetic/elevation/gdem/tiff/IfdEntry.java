/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic.elevation.gdem.tiff;

class IfdEntry {
    public static final IfdEntry INVALID = new IfdEntry((short) 0, IftFieldType.INVALID, 0, 0);

    private final int tag;
    private final IftFieldType type;
    private final int count;
    private final int offset;

    IfdEntry(short tag, IftFieldType type, int count, int offset) {
        this.tag = tag;
        this.type = type;
        this.count = count;
        this.offset = offset;
    }

    public boolean isInvalid() {
        return type == IftFieldType.INVALID;
    }

    public int getTag() {
        return tag;
    }

    public IftFieldType getType() {
        return type;
    }

    public int getCount() {
        return count;
    }

    public int getOffset() {
        return offset;
    }

    @Override
    public String toString() {
        return "IfdEntry{" +
                "tag=" + tag +
                ", type=" + type +
                ", count=" + count +
                ", offset=" + offset +
                '}';
    }
}
