/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic;

import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;

/**
 * Represents a location (latidude, longitude, altitude) in geodetic system.
 */
public class Location {

    private final GeodeticSystem geodeticSystem;
    private final double latitude;
    private final double longitude;
    private final double altitude;

    public Location(GeodeticSystem geodeticSystem, double latitude, double longitude, double altitude) {
        if (geodeticSystem == null) {
            throw new IllegalArgumentException("geodeticSystem cannot be null");
        }
        this.geodeticSystem = geodeticSystem;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    /**
     * Shortcut to create WGS84 location
     *
     * @param latitude  position latitude
     * @param longitude position longitude
     * @param altitude  position altitude
     * @return new location instance
     */
    public static Location wgs84(double latitude, double longitude, double altitude) {
        return new Location(GeodeticSystem.WGS84, latitude, longitude, altitude);
    }

    /**
     * Same as {@link #wgs84(double, double, double)}, but altitude is assumed to be zero
     *
     * @param latitude  position latitude
     * @param longitude position longitude
     * @return new location instance
     */
    public static Location wgs84(double latitude, double longitude) {
        return new Location(GeodeticSystem.WGS84, latitude, longitude, 0);
    }

    /**
     * <pre>
     * Calculates location of the point using two known coordinates and two azimuths from those coordinates.
     *
     * Will throw {@link java.lang.IllegalArgumentException} if geodetic systems of two observers are different.
     * </pre>
     *
     * @param observer1 location of the first observer
     * @param bearing1  bearing to the destination from the first observer
     * @param observer2 location of the second observer
     * @param bearing2  bearing to the destination from the second observer
     * @return location of the point observed by both observers or {@code null}, if there is no such location
     */
    public static Location getLocationByIntersection(Location observer1, double bearing1, Location observer2, double bearing2) {
        if (observer1.getGeodeticSystem() != observer2.getGeodeticSystem()) {
            throw new IllegalArgumentException("Incompatible geodetic systems");
        }

        // see http://williams.best.vwh.net/avform.htm#Intersection
        double lat1 = toRadians(observer1.getLatitude());
        double lat2 = toRadians(observer2.getLatitude());
        double lon1 = toRadians(observer1.getLongitude());
        double lon2 = toRadians(observer2.getLongitude());
        double b13 = toRadians(bearing1);
        double b23 = toRadians(bearing2);

        double dlat = lat2 - lat1;
        double dlon = lon2 - lon1;

        double d12 = 2 * Math.asin(Math.sqrt(Math.sin(dlat / 2) * Math.sin(dlat / 2) +
                Math.cos(lat1) * Math.cos(lat2) * Math.sin(dlon / 2) * Math.sin(dlon / 2)));
        if (d12 == 0) {
            return null;
        }

        // initial/final bearings between points
        double th1 = Math.acos((Math.sin(lat2) - Math.sin(lat1) * Math.cos(d12)) / (Math.sin(d12) * Math.cos(lat1)));
        if (Double.isNaN(th1)) {
            th1 = 0; // protect against rounding
        }
        double th2 = Math.acos((Math.sin(lat1) - Math.sin(lat2) * Math.cos(d12)) / (Math.sin(d12) * Math.cos(lat2)));

        double th12;
        double th21;
        if (Math.sin(lon2 - lon1) > 0) {
            th12 = th1;
            th21 = 2 * Math.PI - th2;
        } else {
            th12 = 2 * Math.PI - th1;
            th21 = th2;
        }

        double a1 = (b13 - th12 + Math.PI) % (2 * Math.PI) - Math.PI; // angle 2-1-3
        double a2 = (th21 - b23 + Math.PI) % (2 * Math.PI) - Math.PI; // angle 1-2-3

        if (Math.sin(a1) == 0 && Math.sin(a2) == 0) return null; // infinite intersections
        if (Math.sin(a1) * Math.sin(a2) < 0) return null;      // ambiguous getLocationByIntersection

        double a3 = Math.acos(-Math.cos(a1) * Math.cos(a2) + Math.sin(a1) * Math.sin(a2) * Math.cos(d12));
        double d13 = Math.atan2(Math.sin(d12) * Math.sin(a1) * Math.sin(a2), Math.cos(a2) + Math.cos(a1) * Math.cos(a3));
        double lat3 = Math.asin(Math.sin(lat1) * Math.cos(d13) + Math.cos(lat1) * Math.sin(d13) * Math.cos(b13));
        double dlon13 = Math.atan2(Math.sin(b13) * Math.sin(d13) * Math.cos(lat1), Math.cos(d13) - Math.sin(lat1) * Math.sin(lat3));
        double lon3 = lon1 + dlon13;
        lon3 = (lon3 + 3 * Math.PI) % (2 * Math.PI) - Math.PI; // normalise to -180..+180º

        return new Location(observer1.getGeodeticSystem(), toDegrees(lat3), toDegrees(lon3), 0);
    }


    public GeodeticSystem getGeodeticSystem() {
        return geodeticSystem;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    /**
     * <pre>
     * Given coordinates of the other point, calculates distance between this and other points and initial and final bearings to the point.
     *
     * Accurate to approximately 0.6 mm, except for nearly antipodal points.
     * </pre>
     *
     * @param location coordinates of target point
     * @return {@link net.ninjacat.geodetic.DistanceAndBearing} containing distance, initial and final bearings to target point
     */
    public DistanceAndBearing getDistanceAndBearing(Location location) {
        return Vincenty.computeDistanceAndBearing(this, location);
    }

    /**
     * <pre>
     * Given initial point, azimuth and distance, calculates coordinates of the target point.
     *
     * Accurate to approximately 0.6 mm, except for nearly antipodal points.
     * </pre>
     *
     * @param distanceAndBearing Distance and bearing to destination. Only {@link net.ninjacat.geodetic.DistanceAndBearing#initialBearing} is used
     * @return coordinates of target location
     */
    public Location getLocationByDistanceAndBearing(DistanceAndBearing distanceAndBearing) {
        return Vincenty.getLocationByDistanceAndBearing(this, distanceAndBearing);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        if (Double.compare(location.altitude, altitude) != 0) return false;
        if (Double.compare(location.latitude, latitude) != 0) return false;
        if (Double.compare(location.longitude, longitude) != 0) return false;
        if (geodeticSystem != location.geodeticSystem) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = geodeticSystem.hashCode();
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(altitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Location{" +
                "geodeticSystem=" + geodeticSystem +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", altitude=" + altitude +
                '}';
    }
}
