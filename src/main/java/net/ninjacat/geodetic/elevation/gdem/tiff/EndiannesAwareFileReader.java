/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic.elevation.gdem.tiff;

import java.io.Closeable;
import java.io.IOException;
import java.io.RandomAccessFile;

class EndiannesAwareFileReader implements Closeable {

    private final RandomAccessFile file;
    private boolean littleEndian;

    EndiannesAwareFileReader(RandomAccessFile file) {
        this.file = file;
        this.littleEndian = false;
    }

    private static short swap(short value) {
        int b1 = value & 0xff;
        int b2 = (value >> 8) & 0xff;

        return (short) (b1 << 8 | b2 << 0);
    }

    private static int swap(int value) {
        int b1 = (value >> 0) & 0xff;
        int b2 = (value >> 8) & 0xff;
        int b3 = (value >> 16) & 0xff;
        int b4 = (value >> 24) & 0xff;

        return b1 << 24 | b2 << 16 | b3 << 8 | b4 << 0;
    }

    private static long swap(long value) {
        long b1 = (value >> 0) & 0xff;
        long b2 = (value >> 8) & 0xff;
        long b3 = (value >> 16) & 0xff;
        long b4 = (value >> 24) & 0xff;
        long b5 = (value >> 32) & 0xff;
        long b6 = (value >> 40) & 0xff;
        long b7 = (value >> 48) & 0xff;
        long b8 = (value >> 56) & 0xff;

        return b1 << 56 | b2 << 48 | b3 << 40 | b4 << 32 |
                b5 << 24 | b6 << 16 | b7 << 8 | b8 << 0;
    }

    private static float swap(float value) {
        int intValue = Float.floatToIntBits(value);
        intValue = swap(intValue);
        return Float.intBitsToFloat(intValue);
    }

    private static double swap(double value) {
        long longValue = Double.doubleToLongBits(value);
        longValue = swap(longValue);
        return Double.longBitsToDouble(longValue);
    }

    private static void swap(short[] array) {
        for (int i = 0; i < array.length; i++)
            array[i] = swap(array[i]);
    }

    public boolean isLittleEndian() {
        return littleEndian;
    }

    public void setLittleEndian(boolean littleEndian) {
        this.littleEndian = littleEndian;
    }

    public void seek(long pos) throws IOException {
        file.seek(pos);
    }

    public long length() throws IOException {
        return file.length();
    }

    public long getFilePointer() throws IOException {
        return file.getFilePointer();
    }

    public int skipBytes(int n) throws IOException {
        return file.skipBytes(n);
    }

    public boolean readBoolean() throws IOException {
        return file.readBoolean();
    }

    public byte readByte() throws IOException {
        return file.readByte();
    }

    public int readUnsignedByte() throws IOException {
        return file.readUnsignedByte();
    }

    public short readShort() throws IOException {
        short value = file.readShort();
        if (littleEndian) {
            value = swap(value);
        }
        return value;
    }

    public int readUnsignedShort() throws IOException {
        int value = file.readUnsignedShort();
        if (littleEndian) {
            value = swap(value) >> 16;
        }
        return value;
    }

    public char readChar() throws IOException {
        return file.readChar();
    }

    public int readInt() throws IOException {
        int value = file.readInt();
        if (littleEndian) {
            value = swap(value);
        }
        return value;
    }

    public byte[] readBytes(int count) throws IOException {
        byte[] data = new byte[count * 4];
        file.readFully(data);
        return data;
    }

    public int[] readInts(int count) throws IOException {
        int[] data = new int[count];
        for (int i = 0; i < count; i++) {
            data[i] = readInt();
        }
        return data;
    }

    public short[] readShorts(int count) throws IOException {
        short[] data = new short[count];
        for (int i = 0; i < count; i++) {
            data[i] = readShort();
        }
        return data;
    }

    public long readLong() throws IOException {
        long value = file.readLong();
        if (littleEndian) {
            value = swap(value);
        }
        return value;
    }

    public float readFloat() throws IOException {
        float value = file.readFloat();
        if (littleEndian) {
            value = swap(value);
        }
        return value;
    }

    public double readDouble() throws IOException {
        double value = file.readDouble();
        if (littleEndian) {
            value = swap(value);
        }
        return value;
    }

    public void close() throws IOException {
        file.close();
    }


}
