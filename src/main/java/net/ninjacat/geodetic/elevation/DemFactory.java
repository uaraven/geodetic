/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic.elevation;

import net.ninjacat.geodetic.elevation.gdem.Gdem;
import net.ninjacat.geodetic.elevation.srtm.Srtm1;
import net.ninjacat.geodetic.elevation.srtm.Srtm3;

public final class DemFactory {

    private DemFactory() {
    }

    public static Dem createForType(DemType demType, String folder) {
        switch (demType) {
            case Srtm1:
                return new Srtm1(folder);
            case Srtm3:
                return new Srtm3(folder);
            case Gdem:
                return new Gdem(folder);
        }
        throw new IllegalArgumentException("Invalid DEM type: " + demType);
    }
}
