/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic.converters;

import net.ninjacat.geodetic.GeodeticConverter;
import net.ninjacat.geodetic.Location;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static net.ninjacat.geodetic.GeodeticSystem.SK42;
import static org.junit.Assert.assertEquals;

public class Wgs84AndSk42ConvertersTest {

    private static final double EPSILON = 0.0000001;
    private static final List<Location> WGS_LOCATIONS = new ArrayList<Location>();
    private static final List<Location> SK_LOCATIONS = new ArrayList<Location>();

    static {
        WGS_LOCATIONS.add(Location.wgs84(50, 50));
        WGS_LOCATIONS.add(Location.wgs84(48.002778, 37.805278));
        WGS_LOCATIONS.add(Location.wgs84(48.566667, 39.333333));

        SK_LOCATIONS.add(new Location(SK42, 49.9998041685, 50.0015220974, 0));
        SK_LOCATIONS.add(new Location(SK42, 48.00278815, 37.80697026, 0));
        SK_LOCATIONS.add(new Location(SK42, 48.56664533, 39.33501892, 0));
    }

    @Test
    public void shouldConvertCoordinatesFromWgs84ToSk42() throws Exception {
        GeodeticConverter converter = new Wgs84ToSk42Converter();
        for (int i = 0; i < WGS_LOCATIONS.size(); i++) {
            Location wgs84 = WGS_LOCATIONS.get(i);
            Location sk42 = converter.convert(wgs84);
            assertEquals(String.format("[%s] Latitude should be calculated correctly", i), SK_LOCATIONS.get(i).getLatitude(), sk42.getLatitude(), EPSILON);
            assertEquals(String.format("[%s] Longitude should be calculated correctly", i), SK_LOCATIONS.get(i).getLongitude(), sk42.getLongitude(), EPSILON);
        }
    }

    @Test
    public void shouldConvertCoordinatesFromSk42ToWgs84() throws Exception {
        GeodeticConverter converter = new Sk42ToWgs84Converter();
        for (int i = 0; i < SK_LOCATIONS.size(); i++) {
            Location sk42 = SK_LOCATIONS.get(i);
            Location wgs84 = converter.convert(sk42);
            assertEquals(String.format("[%s] Latitude should be calculated correctly", i), WGS_LOCATIONS.get(i).getLatitude(), wgs84.getLatitude(), EPSILON);
            assertEquals(String.format("[%s] Longitude should be calculated correctly", i), WGS_LOCATIONS.get(i).getLongitude(), wgs84.getLongitude(), EPSILON);
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailWhenConvertingSk42Location() throws Exception {
        GeodeticConverter converter = new Wgs84ToSk42Converter();
        converter.convert(new Location(SK42, 0, 0, 0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailWhenConvertingWgs84Location() throws Exception {
        GeodeticConverter converter = new Sk42ToWgs84Converter();
        converter.convert(Location.wgs84(0, 0, 0));
    }
}
