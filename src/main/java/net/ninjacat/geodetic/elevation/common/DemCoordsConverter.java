/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic.elevation.common;


public class DemCoordsConverter {

    private final int resolution;
    private final int maxElements;

    public DemCoordsConverter(int resolution, int maxElements) {
        this.resolution = resolution;
        this.maxElements = maxElements;
    }

    public int getMaxElements() {
        return maxElements;
    }

    public int getResolution() {
        return resolution;
    }

    public int limit(int x) {
        if (x < 0) {
            return 0;
        } else if (x > maxElements) {
            return maxElements;
        } else {
            return x;
        }
    }

    public double interpolationFactor(double seconds) {
        double v = seconds / resolution;
        return v - (int) Math.floor(v);
    }

    public int toLinearCoord(double seconds) {
        return (int) Math.floor(seconds / resolution);
    }

    public double extractSeconds(double degree) {
        degree = Math.abs(degree);
        return (degree - (int) degree) * 3600;
    }
}
