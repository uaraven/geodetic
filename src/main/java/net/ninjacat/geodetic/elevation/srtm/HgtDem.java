/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic.elevation.srtm;

import net.ninjacat.geodetic.elevation.common.BasicDem;
import net.ninjacat.geodetic.elevation.common.DemCoordsConverter;
import net.ninjacat.geodetic.elevation.common.DemFileCache;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Generic SRTM model implementation.
 */
public class HgtDem extends BasicDem {
    private static final int VOID_DATA = -32000;

    private static final int MAX_OPEN_HGT_FILES = 4;

    private final DemFileCache<RandomAccessFile> cache;

    public HgtDem(String folder, DemCoordsConverter converter) {
        super(folder, converter);
        cache = new DemFileCache<RandomAccessFile>(MAX_OPEN_HGT_FILES);
    }

    @Override
    public boolean isInvalid(double elevation) {
        return elevation < VOID_DATA;
    }

    @Override
    public void close() {
        cache.closeAndClear();
    }

    @Override
    protected String formatFileName(int lat, int lon, char latD, char lonD) {
        return String.format("%c%02d%c%03d.hgt", latD, lat, lonD, lon);
    }

    @Override
    protected int getElevationForCoords(String fileName, int x, int y) {
        x = getConverter().limit(x);
        y = getConverter().limit(getConverter().getMaxElements() - y);

        RandomAccessFile file;
        if (cache.contains(fileName)) {
            file = cache.get(fileName);
        } else {
            file = openFile(fileName);
            cache.put(fileName, file);
        }
        try {
            file.seek((y * getConverter().getMaxElements() + x) * 2);
            return (int) file.readShort();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private RandomAccessFile openFile(String fileName) {
        try {
            return new RandomAccessFile(fileName, "r");
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

}
