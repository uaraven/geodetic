/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic.elevation.common;

import java.io.Closeable;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DemFileCache<T extends Closeable> {
    private final int maxFiles;
    private final Map<String, T> files = new ConcurrentHashMap<String, T>();
    private final List<String> accessedFiles = new LinkedList<String>();

    public DemFileCache(int maxFiles) {
        this.maxFiles = maxFiles;
    }

    public synchronized boolean contains(String fileName) {
        return files.containsKey(fileName);
    }

    public synchronized void put(String fileName, T file) {
        if (accessedFiles.size() >= maxFiles) {
            evictOldFiles();
        }
        accessedFiles.add(fileName);
        files.put(fileName, file);
    }

    public synchronized T get(String fileName) {
        int index = accessedFiles.indexOf(fileName);
        accessedFiles.remove(index);
        accessedFiles.add(fileName);
        return files.get(fileName);
    }

    public synchronized void closeAndClear() {
        for (T file : files.values()) {
            quitelyCloseFile(file);
        }
        accessedFiles.clear();
        files.clear();
    }

    private void quitelyCloseFile(T file) {
        try {
            file.close();
        } catch (IOException ignored) {
        }
    }

    private void evictOldFiles() {
        while (accessedFiles.size() >= maxFiles) {
            String fileName = accessedFiles.remove(0);
            quitelyCloseFile(files.get(fileName));
            files.remove(fileName);
        }
    }
}
