/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic.elevation.gdem.tiff;

final class Tags {

    public static final short PhotometricInterpretation = 262;
    public static final short Compression = 259;
    public static final short ImageLength = 257;
    public static final short ImageWidth = 256;
    public static final short RowsPerStrip = 278;
    public static final short StripOffsets = 273;
    public static final short StripByteCounts = 279;
    public static final short BitsPerSample = 258;

    private Tags() {
    }
}
