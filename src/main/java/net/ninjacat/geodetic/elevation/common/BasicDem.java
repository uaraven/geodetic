/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic.elevation.common;

import net.ninjacat.geodetic.Location;
import net.ninjacat.geodetic.elevation.Dem;

import java.io.File;

import static net.ninjacat.geodetic.elevation.common.Tools.isFileExists;

public abstract class BasicDem implements Dem {

    private final DemCoordsConverter converter;
    private final String folder;

    protected BasicDem(String folder, DemCoordsConverter converter) {
        if (!isFileExists(folder)) {
            throw new IllegalArgumentException(folder + " does not exist");
        }
        this.folder = folder;
        this.converter = converter;
    }

    /**
     * Returns elevation of location in meters. Elevation is linearly interpolated from closest points of SRTM data file
     *
     * @param location location on Earth
     * @return Ground level in meters
     */
    @Override
    public double getElevation(Location location) {
        String tileFileName = getFileName(location);
        double lon = location.getLongitude();
        double lat = location.getLatitude();
        if (lon < 0) {
            lon = compensateForNegativeCoord(lon);
        }
        if (lat < 0) {
            lat = compensateForNegativeCoord(lat);
        }

        double lonSec = getConverter().extractSeconds(lon);
        double latSec = getConverter().extractSeconds(lat);
        int x1 = getConverter().toLinearCoord(lonSec);
        int y1 = getConverter().toLinearCoord(latSec);
        double xa = getConverter().interpolationFactor(lonSec);
        double ya = getConverter().interpolationFactor(latSec);

        double ele1 = getElevationForCoords(tileFileName, x1, y1);
        double ele2 = getElevationForCoords(tileFileName, x1 + 1, y1);
        double ele3 = getElevationForCoords(tileFileName, x1, y1 + 1);
        double ele4 = getElevationForCoords(tileFileName, x1 + 1, y1 + 1);

        double xele1 = ele1 * (1 - xa) + ele2 * xa;
        double xele2 = ele3 * (1 - xa) + ele4 * xa;

        return xele1 * (1 - ya) + xele2 * ya;
    }

    protected DemCoordsConverter getConverter() {
        return converter;
    }

    protected abstract int getElevationForCoords(String tileFileName, int x, int y);

    protected String getFullName(String fileName) {
        if (folder.endsWith("/")) {
            return folder + fileName;
        } else {
            return folder + File.separator + fileName;
        }
    }

    protected String getFileName(Location location) {
        int lat = (int) Math.abs(location.getLatitude());
        int lon = (int) Math.abs(location.getLongitude());
        if (location.getLongitude() < 0) {
            lon += 1;
        }
        if (location.getLatitude() < 0) {
            lat += 1;
        }
        char latD = location.getLatitude() >= 0 ? 'N' : 'S';
        char lonD = location.getLongitude() >= 0 ? 'E' : 'W';
        String fileName = getFullName(formatFileName(lat, lon, latD, lonD));
        if (isFileExists(fileName)) {
            return fileName;
        }
        throw new IllegalStateException(String.format("%s does not exists for coordinates %s", fileName, location));
    }

    protected abstract String formatFileName(int lat, int lon, char latD, char lonD);

    private double compensateForNegativeCoord(double coord) {
        double coord1 = Math.abs(coord);
        coord1 = (int) coord1 + 1 + (1 - (coord1 - (int) coord1));
        return coord1;
    }

}
