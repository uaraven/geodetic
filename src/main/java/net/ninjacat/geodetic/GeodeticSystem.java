/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic;

public enum GeodeticSystem {
    UNKNOWN(0, 0, 0),
    WGS84(6378137, 6356752.314245, 1 / 298.257223563),
    SK42(6378245, 6356863.019, 1 / 298.3);

    private final double semiMajorAxis;
    private final double semiMinorAxis;
    private final double flattening;
    private final double e2; // first eccentricity squared

    GeodeticSystem(double semiMajorAxis, double semiMinorAxis, double flattening) {
        this.semiMajorAxis = semiMajorAxis;
        this.semiMinorAxis = semiMinorAxis;
        this.flattening = flattening;
        e2 = 2 * flattening - flattening * flattening;
    }

    public double getSemiMajorAxis() {
        return semiMajorAxis;
    }

    public double getSemiMinorAxis() {
        return semiMinorAxis;
    }

    public double getFlattening() {
        return flattening;
    }

    public double getE2() {
        return e2;
    }
}
