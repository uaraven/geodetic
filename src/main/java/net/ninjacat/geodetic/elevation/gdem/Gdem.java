/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic.elevation.gdem;

import net.ninjacat.geodetic.elevation.common.BasicDem;
import net.ninjacat.geodetic.elevation.common.DemCoordsConverter;
import net.ninjacat.geodetic.elevation.common.DemFileCache;
import net.ninjacat.geodetic.elevation.gdem.tiff.Tiff;

import java.io.IOException;

public class Gdem extends BasicDem {
    private static final int MAX_OPEN_HGT_FILES = 4;
    private static final int GDEM_ELEMENTS_PER_FILE = 3601;
    private static final int VALID_ELEVATION_THRESHOLD = 5000;

    private final DemFileCache<Tiff> cache;

    public Gdem(String folder) {
        super(folder, new DemCoordsConverter(1, GDEM_ELEMENTS_PER_FILE));
        cache = new DemFileCache<Tiff>(MAX_OPEN_HGT_FILES);
    }

    @Override
    public boolean isInvalid(double elevation) {
        return elevation < VALID_ELEVATION_THRESHOLD;
    }

    @Override
    public void close() {
        cache.closeAndClear();
    }

    @Override
    protected int getElevationForCoords(String tileFileName, int x, int y) {
        int x1 = getConverter().limit(x);
        int y1 = getConverter().limit(getConverter().getMaxElements() - y);

        try {
            Tiff file;
            if (cache.contains(tileFileName)) {
                file = cache.get(tileFileName);
            } else {
                file = new Tiff(tileFileName);
                cache.put(tileFileName, file);
            }
            return file.getPixelValue(x1, y1);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    protected String formatFileName(int lat, int lon, char latD, char lonD) {
        return String.format("ASTGTM2_%c%02d%c%03d_dem.tif", latD, lat, lonD, lon);
    }

}
