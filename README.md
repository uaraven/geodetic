# Geodetic #

Geodetic functions:

- Get distance and bearing between two points (accuracy up to 0.020" on Earth ellipsoid)
- Get target point by origin coordinates, distance and azimuth (accuracy up to 0.020" on Earth ellipsoid)
- Get point location by location of two observers and their bearing on target point (accuracy about 0.05%)
- Coordinate conversion between WGS84 and SK42 and vice versa
- Ground level for a location on Earth from SRTM3/SRTM1 and ASTER GDEM2 data.

## Elevation ##
  SRTM classes uses .HGT-files as can be downloaded in:
   
  - http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/ for 3" SRTM data 
  - http://dds.cr.usgs.gov/srtm/version2_1/SRTM1/ for 1" SRTM data  
  
  ASTER GDEM uses GeoTIFF files as can be downloaded from http://gdem.ersdac.jspacesystems.or.jp/


# Using #

## Maven ##

Add following dependency to your pom.xml

```
#!xml

<dependencies>
...
  <dependency>
    <groupId>net.ninjacat</groupId>
    <artifactId>geodetic</artifactId>
    <version>0.1</version>
  </dependency>
...
</dependencies>
```

## Gradle ##

```
#!groovy

dependencies {
...
   compile group: 'net.ninjacat', name: 'geodetic', version: '0.1'
...
}
```

## Jar ##

Use following [link](https://bitbucket.org/uaraven/geodetic/downloads/geodetic-0.1.jar) to download Geodetic as a JAR-file.

You can also download [Javadoc](https://bitbucket.org/uaraven/geodetic/downloads/geodetic-0.1-javadoc.jar)