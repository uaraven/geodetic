/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic.elevation;

import net.ninjacat.geodetic.Location;
import net.ninjacat.geodetic.elevation.gdem.Gdem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AsterGdemTest {

    public static final String GDEM_FILES_LOCATION = "./target/test-classes";

    @Test
    public void shouldReadCorrectElevation() throws Exception {
        Gdem gdem = new Gdem(GDEM_FILES_LOCATION);

        Location location = Location.wgs84(43.23494, -79.99470);
        double elevation = gdem.getElevation(location);

        assertEquals("Read elevation should meet expected value", 215, elevation, 20); // ASTER data resolution is 15 to 90 meters
    }

    @Test
    public void shouldReturnCorrectElevationForKatmanduAirport() throws Exception {
        Gdem gdem = new Gdem(GDEM_FILES_LOCATION);

        Location location = Location.wgs84(27.696389, 85.358889);
        double elevation = gdem.getElevation(location);

        assertEquals("Katmandu Airport elevation should meet expected value", 1338, elevation, 30);
    }

    @Test
    public void shouldReturnCorrectElevationForKilimanjaro() throws Exception {
        Gdem gdem = new Gdem(GDEM_FILES_LOCATION);

        Location location = Location.wgs84(-3.075833, 37.353333);
        double elevation = gdem.getElevation(location);

        assertEquals("Read elevation should meet expected value", 5895, elevation, 30);
    }

    @Test
    public void shouldReturnCorrectElevationForRioAirport() throws Exception {
        Gdem gdem = new Gdem(GDEM_FILES_LOCATION);

        Location location = Location.wgs84(-22.914310421030702, -43.16335804760456);
        double elevation = gdem.getElevation(location);

        assertEquals("Read elevation should meet expected value", 3, elevation, 30);
    }

    @Test(expected = IllegalStateException.class)
    public void shouldFailWhenNoFilePresent() throws Exception {
        Gdem gdem = new Gdem(GDEM_FILES_LOCATION);

        Location location = Location.wgs84(50, 50);
        gdem.getElevation(location);
    }
}
