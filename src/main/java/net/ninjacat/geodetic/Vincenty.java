/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic;

import static java.lang.Math.*;

/**
 * Implementation of Vincenty formulae for direct and inverse geodetic problems
 * <p/>
 * Based on http://www.ngs.noaa.gov/PUBS_LIB/inverse.pdf
 * <p>
 * {@link #getLocationByDistanceAndBearing(Location, DistanceAndBearing)} was taken from the Android source code
 * </p>
 */

final class Vincenty {

    private static final int MAXITERS = 40;
    private static final double EPSILON = 1.0e-12;

    private Vincenty() {
    }

    /**
     * Calculates location of the point given initial point, azimuth and distance<br>
     * <p/>
     * Altitude is copied to result from the original point.
     *
     * @param location           initial location
     * @param distanceAndBearing azimuth and distance. Only initialBearing is used
     * @return location of the target point.
     */
    public static Location getLocationByDistanceAndBearing(Location location, DistanceAndBearing distanceAndBearing) {
        // using direct formulae

        double rBearing = toRadians(distanceAndBearing.getInitialBearing());
        double rlat = toRadians(location.getLatitude());
        double rlon = toRadians(location.getLongitude());
        double a = location.getGeodeticSystem().getSemiMajorAxis();
        double b = location.getGeodeticSystem().getSemiMinorAxis();
        double f = location.getGeodeticSystem().getFlattening();
        double dist = distanceAndBearing.getDistance();

        double sina1 = sin(rBearing);
        double cosa1 = cos(rBearing);

        double tanU1 = (1 - f) * tan(rlat);
        double cosU1 = 1 / sqrt(1 + tanU1 * tanU1);
        double sinU1 = tanU1 * cosU1;
        double sigma1 = atan2(tanU1, cosa1);
        double sina = cosU1 * sina1;
        double cosSqa = 1 - sina * sina;
        double uSq = cosSqa * (a * a - b * b) / (b * b);
        double A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
        double B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));

        double sigma = dist / (b * A);
        double sinsigma = 0;
        double cossigma = 0;
        double cos2sM = 0;
        for (int i = 0; i < MAXITERS; i++) {
            cos2sM = cos(2 * sigma1 + sigma);
            sinsigma = sin(sigma);
            cossigma = cos(sigma);
            double dsigma = B * sinsigma * (cos2sM + B / 4 * (cossigma * (-1 + 2 * cos2sM * cos2sM) -
                    B / 6 * cos2sM * (-3 + 4 * sinsigma * sinsigma) * (-3 + 4 * cos2sM * cos2sM)));
            double sigmaPrime = sigma;
            sigma = dist / (b * A) + dsigma;
            if (abs(sigma - sigmaPrime) < EPSILON) {
                break;
            }
        }

        double tmp = sinU1 * sinsigma - cosU1 * cossigma * cosa1;
        double reslat = atan2(sinU1 * cossigma + cosU1 * sinsigma * cosa1, (1 - f) * sqrt(sina * sina + tmp * tmp));
        double lambda = atan2(sinsigma * sina1, cosU1 * cossigma - sinU1 * sinsigma * cosa1);
        double C = f / 16 * cosSqa * (4 + f * (4 - 3 * cosSqa));
        double L = lambda - (1 - C) * f * sina * (sigma + C * sinsigma * (cos2sM + C * cossigma * (-1 + 2 * cos2sM * cos2sM)));
        double reslon = (rlon + L + 3 * PI) % (2 * PI) - PI;  // normalise to -180...+180

        return new Location(location.getGeodeticSystem(), toDegrees(reslat), toDegrees(reslon), location.getAltitude());
    }

    /**
     * Calculates distance and bearing between to points.<br>
     * Geodetic systems of both points must be the same
     *
     * @param location1 first point
     * @param location2 second point
     * @return distance and bearings
     */
    static DistanceAndBearing computeDistanceAndBearing(Location location1, Location location2) {
        // using the "Inverse Formula" (section 4)

        if (location1.getGeodeticSystem() != location2.getGeodeticSystem()) {
            throw new IllegalArgumentException("Geodetic systems must match");
        }

        // Convert lat/long to radians
        double lat1 = toRadians(location1.getLatitude());
        double lat2 = toRadians(location2.getLatitude());
        double lon1 = toRadians(location1.getLongitude());
        double lon2 = toRadians(location2.getLongitude());

        double a = location1.getGeodeticSystem().getSemiMajorAxis(); // WGS84 major axis
        double b = location1.getGeodeticSystem().getSemiMinorAxis(); // WGS84 semi-major axis
        double f = location1.getGeodeticSystem().getFlattening();
        double aSqMinusBSqOverBSq = (a * a - b * b) / (b * b);

        double L = lon2 - lon1;
        double A = 0.0;
        double U1 = atan((1.0 - f) * tan(lat1));
        double U2 = atan((1.0 - f) * tan(lat2));

        double cosU1 = cos(U1);
        double cosU2 = cos(U2);
        double sinU1 = sin(U1);
        double sinU2 = sin(U2);
        double cosU1cosU2 = cosU1 * cosU2;
        double sinU1sinU2 = sinU1 * sinU2;

        double sigma = 0.0;
        double deltaSigma = 0.0;
        double cosSqAlpha;
        double cos2SM;
        double cosSigma;
        double sinSigma;
        double cosLambda = 0.0;
        double sinLambda = 0.0;

        double lambda = L; // initial guess
        for (int iter = 0; iter < MAXITERS; iter++) {
            double lambdaOrig = lambda;
            cosLambda = cos(lambda);
            sinLambda = sin(lambda);
            double t1 = cosU2 * sinLambda;
            double t2 = cosU1 * sinU2 - sinU1 * cosU2 * cosLambda;
            double sinSqSigma = t1 * t1 + t2 * t2; // (14)
            sinSigma = sqrt(sinSqSigma);
            cosSigma = sinU1sinU2 + cosU1cosU2 * cosLambda; // (15)
            sigma = atan2(sinSigma, cosSigma); // (16)
            double sinAlpha = (sinSigma == 0) ? 0.0 :
                    cosU1cosU2 * sinLambda / sinSigma; // (17)
            cosSqAlpha = 1.0 - sinAlpha * sinAlpha;
            cos2SM = (cosSqAlpha == 0) ? 0.0 :
                    cosSigma - 2.0 * sinU1sinU2 / cosSqAlpha; // (18)

            double uSquared = cosSqAlpha * aSqMinusBSqOverBSq; // defn
            A = 1 + (uSquared / 16384.0) * // (3)
                    (4096.0 + uSquared *
                            (-768 + uSquared * (320.0 - 175.0 * uSquared)));
            double B = (uSquared / 1024.0) * // (4)
                    (256.0 + uSquared *
                            (-128.0 + uSquared * (74.0 - 47.0 * uSquared)));
            double C = (f / 16.0) *
                    cosSqAlpha *
                    (4.0 + f * (4.0 - 3.0 * cosSqAlpha)); // (10)
            double cos2SMSq = cos2SM * cos2SM;
            deltaSigma = B * sinSigma * // (6)
                    (cos2SM + (B / 4.0) *
                            (cosSigma * (-1.0 + 2.0 * cos2SMSq) -
                                    (B / 6.0) * cos2SM *
                                            (-3.0 + 4.0 * sinSigma * sinSigma) *
                                            (-3.0 + 4.0 * cos2SMSq)));

            lambda = L +
                    (1.0 - C) * f * sinAlpha *
                            (sigma + C * sinSigma *
                                    (cos2SM + C * cosSigma *
                                            (-1.0 + 2.0 * cos2SM * cos2SM))); // (11)

            double delta = (lambda - lambdaOrig) / lambda;
            if (abs(delta) < EPSILON) {
                break;
            }
        }

        float distance = (float) (b * A * (sigma - deltaSigma));
        float initialBearing = (float) atan2(cosU2 * sinLambda, cosU1 * sinU2 - sinU1 * cosU2 * cosLambda);
        initialBearing *= 180.0 / PI;
        float finalBearing = (float) atan2(cosU1 * sinLambda, -sinU1 * cosU2 + cosU1 * sinU2 * cosLambda);
        finalBearing *= 180.0 / PI;
        return new DistanceAndBearing(distance, initialBearing, finalBearing);
    }

}
