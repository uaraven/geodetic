/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic;

import net.ninjacat.geodetic.converters.Conversion;
import net.ninjacat.geodetic.converters.Sk42ToWgs84Converter;
import net.ninjacat.geodetic.converters.Wgs84ToSk42Converter;

import java.util.HashMap;
import java.util.Map;

import static net.ninjacat.geodetic.GeodeticSystem.SK42;
import static net.ninjacat.geodetic.GeodeticSystem.WGS84;

/**
 * Factory for a {@link net.ninjacat.geodetic.GeodeticConverter}
 */
public final class GeodeticConverterFactory {

    private static final Map<Conversion, GeodeticConverter> CONVERTERS = new HashMap<Conversion, GeodeticConverter>();

    static {
        CONVERTERS.put(new Conversion(WGS84, SK42), new Wgs84ToSk42Converter());
        CONVERTERS.put(new Conversion(SK42, WGS84), new Sk42ToWgs84Converter());
    }

    private GeodeticConverterFactory() {
    }

    /**
     * Creates a coverter for coordinate transformation between different geodetic systems
     *
     * @param from original geodetic system
     * @param to   target geodetic system
     * @return {@link net.ninjacat.geodetic.GeodeticConverter}
     * @throws NoConversionException if no conversion available
     */
    public static GeodeticConverter getConverter(GeodeticSystem from, GeodeticSystem to) throws NoConversionException {
        Conversion conversion = new Conversion(from, to);
        if (CONVERTERS.containsKey(conversion)) {
            return CONVERTERS.get(conversion);
        }
        throw new NoConversionException(String.format("No conversion from %s to %s", from, to));
    }
}
