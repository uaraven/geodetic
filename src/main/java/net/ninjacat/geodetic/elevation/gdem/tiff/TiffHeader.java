/*
 * Copyright 2014 Oleksiy Voronin <ovoronin@gmail.com>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ninjacat.geodetic.elevation.gdem.tiff;

import java.io.IOException;

class TiffHeader {
    private static final int LITTLE_ENDIAN_MAGIC = 0x4949;
    private final boolean littleEndian;
    private final int ifdOffset;

    TiffHeader(EndiannesAwareFileReader file) throws IOException {
        file.seek(0);
        short byteOrder = file.readShort();

        littleEndian = byteOrder == LITTLE_ENDIAN_MAGIC;
        file.setLittleEndian(littleEndian);
        file.readShort();
        ifdOffset = file.readInt();
    }

    public boolean isLittleEndian() {
        return littleEndian;
    }

    public int getIfdOffset() {
        return ifdOffset;
    }
}
